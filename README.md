SNEAKERHEADZ

Heroku Link: https://sneakerheadz.herokuapp.com/

SneakerHeadz is an application in which a user can create 'shoeboxes' containing their collection of sneakers.  You can check out other users sneakers, add them to your wishlist and eventually buy and/or trade.

Future Development
Different Search by year, brand, release
Payment capabilities
Messages between usersAbility to add to wishlist

Technologies Used
Heroku | Node.js | MongoDB | Mongoose | JavaScript | styled-components | Trello | Imgur | VS Code Editor | React.js | Axios | 


